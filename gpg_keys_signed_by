#!/usr/bin/python3

# Copyright (c) 2019, Jean-Benoist Leger <jb@leger.tf>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import subprocess
import argparse
import sys


def get_keys(gpg_exec, ref_keys):
    so = subprocess.Popen(
        [gpg_exec, "--no-auto-check-trustdb", "--list-sigs", "--with-colons"],
        stdout=subprocess.PIPE,
    )

    current_pub = None

    keys = set()
    while True:
        one_line = so.stdout.readline()
        if one_line == b"":
            break
        splitted_line = one_line.split(b":")
        if splitted_line[0] == b"pub":
            current_pub = splitted_line[4].decode("utf8")
        if splitted_line[0] == b"sig":
            signer = splitted_line[4].decode('utf8')
            if signer in ref_keys:
                if current_pub not in keys:
                    keys.add(current_pub)
                    yield current_pub


def main():
    parser = argparse.ArgumentParser(
        description="Find keys signed by a given key",
    )

    parser.add_argument(
        "-g", "--gpg", dest="gpg_exec", default="gpg", help="Command to execute gpg"
    )

    parser.add_argument(
        dest="long_keyid",
        nargs=1,
        type=str,
        help="long id of the reference key",
    )

    args = parser.parse_args()

    for k in get_keys(args.gpg_exec, args.long_keyid):
        print(k)


if __name__ == "__main__":
    main()
